package com.magnit.testapp.chekushova;

import com.magnit.testapp.chekushova.service.EntryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

import static java.lang.Integer.parseInt;
import static java.lang.System.currentTimeMillis;

public class TestApp {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestApp.class);
    private String host;
    private String base;
    private String user;
    private String pass;
    private int number;

    public static void main(String[] args) {
        TestApp app = new TestApp();
        app.startScript();
    }

    private void startScript() {
        try {
            long startTime = currentTimeMillis();
            init();
            EntryService service = new EntryService();
            service.initDataSource(host, base, user, pass);
            service.createTable();
            service.clearTable();
            service.insertData(number);
            service.marshalingEntries(service.getData());
            service.transformEntries();
            service.parseDocument();
            long finalTime = currentTimeMillis() - startTime;
            int seconds = (int) (finalTime / 1000.0) % 60;
            int minutes = (int) (finalTime / (1000.0 * 60)) % 60;
            LOGGER.info("Program execution time {} minutes {} seconds.", minutes, seconds);
        } catch (SQLException | IOException | JAXBException | TransformerException e) {
            LOGGER.error(e.toString(), e);
        }
    }

    private void init() throws IOException {
        Properties props = new Properties();
        props.load(this.getClass().getClassLoader().getResourceAsStream("application.properties"));
        setHost(props.getProperty("database.host"));
        setBase(props.getProperty("database.base"));
        setUser(props.getProperty("database.user"));
        setPass(props.getProperty("database.password"));
        setNumber(parseInt(props.getProperty("entries.number")));
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }
}