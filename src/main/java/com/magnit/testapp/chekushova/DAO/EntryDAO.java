package com.magnit.testapp.chekushova.DAO;

import com.magnit.testapp.chekushova.domain.Entry;
import java.sql.Connection;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class EntryDAO {
    private static final String CREATE_DB = "CREATE DATABASE IF NOT EXISTS magnit";
    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS Entry (field INT)";
    private static final String INSERT_ENTRIES = "INSERT INTO entry (field) values (?)";
    private static final String DELETE_ENTRIES = "DELETE FROM entry";
    private static final String SELECT_ENTRIES = "SELECT * FROM Entry";
    private Connection connection;

    public EntryDAO(Connection connection) {
        this.connection = connection;
    }

    public void createDatabase() throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(CREATE_DB)) {
            statement.executeUpdate();
        }
    }

    public void initDatabase() throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(CREATE_TABLE)) {
            statement.executeUpdate();
        }
    }

    public void insetData(List<Entry> entries) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(INSERT_ENTRIES)) {
            for (Entry entry : entries) {
                statement.setInt(1, entry.getField());
                statement.addBatch();
            }
            statement.executeBatch();
        }
    }

    public List<Entry> getData() throws SQLException {
        List<Entry> entries = new LinkedList<>();
        try (PreparedStatement statement = connection.prepareStatement(SELECT_ENTRIES)) {
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                entries.add(createEntryFromResult(set));
            }
        }
        return entries;
    }

    public void clearData() throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(DELETE_ENTRIES)) {
            statement.executeUpdate();
        }
    }

    private Entry createEntryFromResult(ResultSet resultSet) throws SQLException {
        Entry entry = new Entry();
        entry.setField(resultSet.getInt("Field"));
        return entry;
    }
}
