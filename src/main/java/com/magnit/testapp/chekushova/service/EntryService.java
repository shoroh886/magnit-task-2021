package com.magnit.testapp.chekushova.service;

import com.magnit.testapp.chekushova.DAO.EntryDAO;
import com.magnit.testapp.chekushova.domain.Entry;
import com.magnit.testapp.chekushova.service.dto.Entries;
import com.magnit.testapp.chekushova.service.dto.EntriesField;
import com.magnit.testapp.chekushova.service.dto.EntryField;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static java.sql.DriverManager.getConnection;

public class EntryService {
    private static final Logger LOGGER = LoggerFactory.getLogger(EntryService.class);
    private static final ThreadLocal<Connection> CONNECTION = new ThreadLocal<>();
    private static final Random RANDOM = new Random();

    private MysqlDataSource dataSource;

    public void initDataSource(String host, String dataBase, String user, String pass) throws SQLException {
        try {
            LOGGER.info("Initial database. host: {}, db: {}, user: {},  password: {}.", host, dataBase, user, pass);
            CONNECTION.set(getConnection(host, user, pass));
            CONNECTION.get().setAutoCommit(false);
            EntryDAO dao = new EntryDAO(CONNECTION.get());
            dao.createDatabase();
            String url = host + dataBase + "?useSSL=false";
            dataSource = new MysqlDataSource();
            dataSource.setUrl(url);
            dataSource.setUser(user);
            dataSource.setPassword(pass);
        } catch (SQLException e) {
            CONNECTION.get().rollback();
            LOGGER.error(e.toString(), e);
        } finally {
            CONNECTION.get().commit();
            CONNECTION.get().close();
        }
    }

    public void createTable() throws SQLException {
        try {
            LOGGER.debug("Initial entries table.");
            CONNECTION.set(dataSource.getConnection());
            CONNECTION.get().setAutoCommit(false);
            EntryDAO dao = new EntryDAO(CONNECTION.get());
            dao.initDatabase();
        } catch (SQLException e) {
            CONNECTION.get().rollback();
            LOGGER.error(e.toString(), e);
        } finally {
            CONNECTION.get().commit();
            CONNECTION.get().close();
        }
    }

    public void insertData(int number) throws SQLException {
        try {
            LOGGER.info("Insert {} values into database.", number);
            CONNECTION.set(dataSource.getConnection());
            CONNECTION.get().setAutoCommit(false);
            EntryDAO dao = new EntryDAO(CONNECTION.get());
            dao.insetData(createEntries(number));
        } catch (SQLException e) {
            CONNECTION.get().rollback();
            LOGGER.error(e.toString(), e);
        } finally {
            CONNECTION.get().commit();
            CONNECTION.get().close();
        }
    }

    public List<Entry> getData() throws SQLException {
        try {
            LOGGER.debug("Select entries.");
            CONNECTION.set(dataSource.getConnection());
            CONNECTION.get().setAutoCommit(false);
            EntryDAO dao = new EntryDAO(CONNECTION.get());
            return dao.getData();
        } catch (SQLException e) {
            CONNECTION.get().rollback();
            LOGGER.error(e.toString(), e);
        } finally {
            CONNECTION.get().close();
        }
        return new LinkedList<>();
    }

    public void clearTable() throws SQLException {
        try {
            LOGGER.debug("Clearing table.");
            CONNECTION.set(dataSource.getConnection());
            CONNECTION.get().setAutoCommit(false);
            EntryDAO dao = new EntryDAO(CONNECTION.get());
            dao.clearData();
        } catch (SQLException e) {
            CONNECTION.get().rollback();
            LOGGER.error(e.toString(), e);
        } finally {
            CONNECTION.get().commit();
            CONNECTION.get().close();
        }
    }

    public void transformEntries() throws TransformerException {
        LOGGER.debug("Transform document.");
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        Source article = new StreamSource(getClass().getResourceAsStream("/article.xsl"));
        Transformer transformer = factory.newTransformer(article);
        Source xml = new StreamSource(new File("output/1.xml"));
        transformer.transform(xml, new StreamResult(new File("output/2.xml")));
    }

    public void marshalingEntries(List<Entry> entriesList) throws JAXBException {
        LOGGER.info("Marshaling {} values.", entriesList.size());
        new File("output").mkdir();
        Entries entries = new Entries();
        entries.setEntries(entriesList);
        JAXBContext context = JAXBContext.newInstance(Entries.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(entries, new File("output/1.xml"));
    }

    public void parseDocument() throws JAXBException {
        LOGGER.debug("Unmarshal document.");
        JAXBContext context = JAXBContext.newInstance(EntriesField.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        EntriesField entries = (EntriesField) unmarshaller.unmarshal(new File("output/2.xml"));
        LOGGER.info("Sum of fields = {}.", entries.getEntries().stream().mapToInt(EntryField::getField).sum());
        CONNECTION.remove();
    }

    private List<Entry> createEntries(int number) {
        List<Entry> entries = new LinkedList<>();
        for (int i = 0; i < number; i++) {
            Entry entry = new Entry(getRandomValue());
            LOGGER.trace("Create entry: {}.", entry);
            entries.add(entry);
        }
        return entries;
    }

    public int getRandomValue() {
        return RANDOM.nextInt(100);
    }
}